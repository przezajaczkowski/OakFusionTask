package org.psoft;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class Calculator {

	private static final Pattern PATTERN = Pattern.compile(" ");

	private final StringsToIntsConverter stringsToIntsConverter;
	private final List<Integer> negativeValuesList = new LinkedList<>();
	private boolean negativesNotDetected = true;

	public Calculator(StringsToIntsConverter stringsToIntsConverter) {
		this.stringsToIntsConverter = stringsToIntsConverter;
	}

	public int add(String numbers) {
		if (isBlank(numbers)) {
			return 0;
		}

		return stringsToIntsConverter.anIntegerStreamFrom(numbers)
				.filter(value -> value < 1000)
				.peek(this::peekNegativeValues)
				.reduce((a, b) -> a + b)
				.filter(value -> negativesNotDetected)
				.orElseThrow(() -> new RuntimeException("negatives not allowed: " + negativeValuesList.toString()));
	}

	private void peekNegativeValues(Integer value) {
		if (value < 0) {
			negativeValuesList.add(value);
			negativesNotDetected = false;
		}
	}
}
