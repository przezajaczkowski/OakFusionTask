package org.psoft;

class Input {
	private String header;
	private String body;

	Input(String header, String body) {
		this.header = header;
		this.body = body;
	}

	public String getHeader() {
		return header;
	}

	public String getBody() {
		return body;
	}
}
