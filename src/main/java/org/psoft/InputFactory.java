package org.psoft;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class InputFactory {
	private final Pattern HEADER_PATTERN = Pattern.compile("^(//(\\[?[^a-zA-Z0-9]*\\])*\\n)(.*)");

	public Input anInputFrom(String incoming) {
		Matcher matcher = HEADER_PATTERN.matcher(incoming);
		if (matcher.find()) {
			String header = matcher.group(1);
			String body = matcher.group(matcher.groupCount());
			return new Input(header, body);
		}
		return new Input(null, incoming);
	}
}
