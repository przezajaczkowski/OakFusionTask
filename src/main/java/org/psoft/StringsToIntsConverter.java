package org.psoft;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;
import java.util.stream.Stream;

public class StringsToIntsConverter {

	private static final String DEFAULT_DELIMITERS = " |,|\\n";
	private static final Pattern DEFAULT_PATTERN = Pattern.compile(DEFAULT_DELIMITERS);
	private static final Pattern START_HEADER_DELIMITER_PATTERN = Pattern.compile("^//");
	private static final Pattern END_HEADER_DELIMITER_PATTERN = Pattern.compile("\\n$");
	private static final Pattern DELIMITERS_SPLITTING_PATTERN = Pattern.compile("^\\[|\\]\\[|\\]");

	private InputFactory inputFactory;

	public StringsToIntsConverter(InputFactory inputFactory) {
		this.inputFactory = inputFactory;
	}

	Stream<Integer> anIntegerStreamFrom(String source) {
		Input input = inputFactory.anInputFrom(source);

		if (isDelimiterHeaderPresent(input)) {
			return mapWithCustomDelimiter(input);
		}
		return mapToIntegerStream(DEFAULT_PATTERN, input.getBody());
	}

	private boolean isDelimiterHeaderPresent(Input input) {
		return input.getHeader() != null;
	}

	private Stream<Integer> mapWithCustomDelimiter(Input input) {
		return Stream.of(headerSourcedDelimiterRegexp(input.getHeader()))
				.map(delimiterRegexp -> DEFAULT_DELIMITERS + "|" + delimiterRegexp)
				.map(Pattern::compile)
				.flatMap(pattern -> mapToIntegerStream(pattern, input.getBody()));
	}

	private Stream<Integer> mapToIntegerStream(Pattern pattern, String input) {
		return pattern.splitAsStream(input)
				.map(Integer::valueOf);
	}

	public String headerSourcedDelimiterRegexp(String input) {
		String clearedFromStartTag = cleanInputFrom(input, START_HEADER_DELIMITER_PATTERN);
		String clearedFromEndTag = cleanInputFrom(clearedFromStartTag, END_HEADER_DELIMITER_PATTERN);
		String[] elements = Stream.of(DELIMITERS_SPLITTING_PATTERN.split(clearedFromEndTag))
				.filter(StringUtils::isNotBlank)
				.toArray(String[]::new);
		return String.join("|", elements);
	}

	private String cleanInputFrom(String input, Pattern pattern) {
		return pattern.matcher(input).replaceFirst(StringUtils.EMPTY);
	}
}