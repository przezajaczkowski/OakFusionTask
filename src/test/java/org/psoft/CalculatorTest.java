package org.psoft;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

public class CalculatorTest {

	private static final String SOME_INPUT = "someInput";

	private StringsToIntsConverter stringsToIntsConverter;
	private Calculator calculator;

	@BeforeEach
	void setUp() {
		stringsToIntsConverter = Mockito.mock(StringsToIntsConverter.class);
		calculator = new Calculator(stringsToIntsConverter);
	}

	@ParameterizedTest
	@ValueSource(strings = {"", " "})
	void shouldReturnZeroForEmptyString(String emptyInput) {
		assertThat(calculator.add(emptyInput)).isEqualTo(0);
	}

	@ParameterizedTest
	@MethodSource("testSumSources")
	void shouldReturnSumOfGivenNumbers(Stream<Integer> numbers, int result) {
		given(stringsToIntsConverter.anIntegerStreamFrom(SOME_INPUT)).willReturn(numbers);

		assertThat(calculator.add(SOME_INPUT)).isEqualTo(result);

		then(stringsToIntsConverter).should().anIntegerStreamFrom(SOME_INPUT);
		then(stringsToIntsConverter).shouldHaveNoMoreInteractions();
	}

	static Stream<Arguments> testSumSources() {
		return Stream.of(
				Arguments.of(Stream.of(0), 0),
				Arguments.of(Stream.of(0, 1), 1),
				Arguments.of(Stream.of(0, 1, 4), 5),
				Arguments.of(Stream.of(0, 1, 1000), 1)
		);
	}

	@Test
	void shouldThrowExceptionWhenNegativeNumberIsPresent() {
		given(stringsToIntsConverter.anIntegerStreamFrom(SOME_INPUT)).willReturn(Stream.of(0, 1, -2, -100, -23));

		Throwable throwable = catchThrowable(() -> calculator.add(SOME_INPUT));

		assertThat(throwable)
				.isInstanceOf(RuntimeException.class)
				.hasMessage("negatives not allowed: [-2, -100, -23]");
	}
}