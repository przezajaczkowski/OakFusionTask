package org.psoft;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

class InputFactoryTest {

	InputFactory inputFactory = new InputFactory();

	@Test
	void shouldCreateInputWithBodyAndHeader() {
		Input input = inputFactory.anInputFrom("//[;]\n1234");

		assertThat(input.getBody()).isEqualTo("1234");
		assertThat(input.getHeader()).isEqualTo("//[;]\n");
	}

	@Test
	void shouldCreateInputWithBodyWhenHeaderIsEmpty() {
		Input input = inputFactory.anInputFrom("1234");

		assertThat(input.getBody()).isEqualTo("1234");
		assertThat(input.getHeader()).isNull();
	}

	@Test
	void shouldCreateInputWithBodyAndMultipleHaders() {
		Input input = inputFactory.anInputFrom("//[;][.][&]\n1,2,3,4");

		assertThat(input.getBody()).isEqualTo("1,2,3,4");
		assertThat(input.getHeader()).isEqualTo("//[;][.][&]\n");
	}

	@Test
	void shouldCreateInputWithBodyAndMultiCharHeaders() {
		Input input = inputFactory.anInputFrom("//[;][...][&]\n1234");

		assertThat(input.getBody()).isEqualTo("1234");
		assertThat(input.getHeader()).isEqualTo("//[;][...][&]\n");
	}
}