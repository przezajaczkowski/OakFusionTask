package org.psoft;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class StringsToIntsConverterTest {

	private InputFactory inputFactory = mock(InputFactory.class);
	private Input inputMock = mock(Input.class);
	private StringsToIntsConverter stringsToIntsConverter;

	@BeforeEach
	void setUp() {
		stringsToIntsConverter = new StringsToIntsConverter(inputFactory);

		given(inputFactory.anInputFrom(any())).willReturn(inputMock);
	}

	@ParameterizedTest
	@MethodSource("correctInputSoruce")
	void shouldSplitValuesByComma(String source) {
		given(inputMock.getBody()).willReturn(source);
		given(inputMock.getHeader()).willReturn(null);

		Stream<Integer> integerStream = stringsToIntsConverter.anIntegerStreamFrom("someString");

		assertThat(integerStream).containsExactly(1, 2, 3, 4, 5);
	}

	static Stream<String> correctInputSoruce() {
		return Stream.of(
				"1,2,3,4,5",
				"1 2 3 4 5",
				"1\n2\n3\n4\n5",
				"1,2 3\n4\n5"
		);
	}

	@Test
	void shouldSplitValuesByDelimiterInHeader() {
		given(inputMock.getBody()).willReturn("1;2;3;4;5");
		given(inputMock.getHeader()).willReturn("//;\n");

		Stream<Integer> integerStream = stringsToIntsConverter.anIntegerStreamFrom("someString");

		assertThat(integerStream).containsExactly(1, 2, 3, 4, 5);
	}

	@Test
	void shouldSplitValuesByMixedHeaderAndDefaultDelimiters() {
		given(inputMock.getBody()).willReturn("1 2,3;4\n5");
		given(inputMock.getHeader()).willReturn("//;\n");

		Stream<Integer> integerStream = stringsToIntsConverter.anIntegerStreamFrom("someString");

		assertThat(integerStream).containsExactly(1, 2, 3, 4, 5);
	}

	@Test
	void shouldSplitValuesByCustomDelimiterInHeader() {
		given(inputMock.getBody()).willReturn("1;2;3;4;5");
		given(inputMock.getHeader()).willReturn("//[;]\n");

		Stream<Integer> integerStream = stringsToIntsConverter.anIntegerStreamFrom("someString");

		assertThat(integerStream).containsExactly(1, 2, 3, 4, 5);
	}

	@Test
	void shouldSplitValuesByMultipleCustomDelimitersInHeader() {
		given(inputMock.getBody()).willReturn("1;2#3@4;5");
		given(inputMock.getHeader()).willReturn("//[;][#][@]\n");

		Stream<Integer> integerStream = stringsToIntsConverter.anIntegerStreamFrom("someString");

		assertThat(integerStream).containsExactly(1, 2, 3, 4, 5);
	}

	@Test
	void shouldSplitValuesByMulticharDelimiters() {
		given(inputMock.getBody()).willReturn("1;2###3@4;5");
		given(inputMock.getHeader()).willReturn("//[;][###][@]\n");

		Stream<Integer> integerStream = stringsToIntsConverter.anIntegerStreamFrom("someString");

		assertThat(integerStream).containsExactly(1, 2, 3, 4, 5);
	}
}